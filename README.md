# Okefabel

Sebuah proyek donasi yang bertujuan untuk menyalurkan bantun bagi para difabel yang membutuhkan.

Proyek ini didukung oleh BCA Synrgy Academy x Binar Academy


## Built by

### Front-End Developer:
* Muhammad Nuh Ibnu Aimar
* Yemima Inadia
* Irma Puji AS
* Hizkirani Jatiningrum

## Built with

### JavaScript

Bahasa pemrograman yang dipakai adalah JavaScript. 
Kelebihan dari bahasa ini tidak hanya dapat digunakan untuk website pada sisi pengguna saja, melainkan juga pada sisi server. 
Nilai plus ini kami manfaatkan untuk memudahkan kami untuk berjalan bersama dengan sisi *back-end*. 

### React 

Penggunaan React sebagai dasar pengembangan aplikasi ini dipilih atas dasar kemudahan implementasi task UI secara efisien.
Komponen React juga lebih mudah untuk ditulis karena menggunakan JSX yang memudahkan kami dalam pengerjaan tiap fitur.
Untuk awal pengerjaan kami menggunakan fitur `create-react-app` 

### Redux

Untuk *state management*, kami telah mengimplementasikan Redux ke dalam area pokok seperti filtering, sorting data produk donasi untuk memudahkan 
developer menyimpan *state* yang akan digunakan di banyak komponen. 

### Axios

Untuk pemanggilan API, Axios kami gunakan untuk berkomunikasi dengan Back-end.

### Bootstrap

Dari segi *design*, fitur-fitur yang telah disediakan React-bootstrap cukup membantu dalam menyediakan kebutuhan *styling* 
dalam waktu singkat

### Moment

Untuk memudahkan kami dalam mem-parsing, memvalidasi, memanipulasi serta menampilkan tanggal dan waktu dengan Javascript.

### Font Awesome

*Font Awesome* sebagai sumber ikon vektor terukur memudahkan developer dalam implementasi desain.

### Dependencies lainnya

Selain *dependencies* di atas, ada banyak *dependencies* lain yang kami gunakan untuk menunjang aplikasi ini baik dari segi desain 
maupun fungsionalitasnya. Berikut merupakan daftar lengkap *dependencies* yang kami gunakan:

* @feuer/react-tabs
* @fortawesome/free-solid-svg-icons
* axios
* bootstrap
* font-awesome
* jwt-decode
* moment
* react
* react-bootstrap
* react-copy-to-clipboard
* react-datepicker
* react-dom
* react-jwt-auth
* react-moment
* react-redux
* react-responsive-carousel
* react-router-dom
* react-scripts
* react-scroll
* react-slick
* react-truncate-html
* redux
* redux-thunk
* slick-carousel
* styled-components
